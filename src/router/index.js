import Vue from 'vue'
import VueRouter from 'vue-router'
import Nav from '../views/layout/Nav.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    components: {
      nav: Nav,
      default: () => import(/* webpackChunkName: "home" */ '../views/Home/Home.vue')
    }
  },
  {
    path: '/hot',
    name: 'hot',
    components: {
      nav :Nav,
      default: () => import(/* webpackChunkName: "hot" */ '../views/Hot.vue')
    }
  },
  {
    path: '/user',
    name: 'user',
    components: {
      nav :Nav,
      default: () => import(/* webpackChunkName: "user" */ '../views/User/User.vue')
    },
    beforeEnter(to, from, next){
      // 读取本地存储中关于登录状态的信息并解析
      const isLogin = localStorage.getItem("isLogin") ? JSON.parse(localStorage.getItem("isLogin")) : {};
      // 根据指定信息，判断是否登录
      if(isLogin.state === "ok"){
        // 登录了，通过
        next();
      }else{
        // 未登录，去登录
        next("/login");
      }
    }
  },
  {
    path: '/search',
    name: 'search',
    components: {
      nav :Nav,
      default: () => import(/* webpackChunkName: "search" */ '../views/Search.vue')
    }
  },
  {
    path: '/login',
    name: 'login',
    components: {
      default: () => import(/* webpackChunkName: "user" */ '../views/User/Login.vue')
    }
  },
  {
    path: '/register',
    name: 'register',
    components: {
      default: () => import(/* webpackChunkName: "user" */ '../views/User/Register.vue')
    }
  },
  {
    path: '/SongList/:id',
    name: 'SongList',
    components: {
      default: () => import(/* webpackChunkName: "home" */ '../views/Home/SongList.vue')
    }
  },
  {
    path: '/SongPlay/:id',
    name: 'SongPlay',
    components: {
      default: () => import(/* webpackChunkName: "SongPlay" */ '../views/SongPlay.vue')
    }
  }
]



const router = new VueRouter({
  routes
})

export default router
