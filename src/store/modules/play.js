export default {
    state:{
        imgUrl:"",
        mName:"",
        mAdmin:"",
        cTime:0,
        duration:0,
        isPlay:false,
        mUrl:"",
        isShow:false
    },
    mutations:{
        change_imgUrl(state, pl){
            state.imgUrl = pl;
        },
        change_mName(state, pl){
            state.mName = pl;
        },
        change_mAdmin(state, pl){
            state.mAdmin = pl;
        },
        change_cTime(state, pl){
            state.cTime = pl;
        },
        change_duration(state, pl){
            state.duration = pl;
        },
        change_isPlay(state, pl){
            state.isPlay = pl;
        },
        change_mUrl(state, pl){
            state.mUrl = pl;
        },
        change_isShow(state, pl){
            state.isShow = pl;
        },
    },
    namespaced: true
}