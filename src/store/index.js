import Vue from 'vue'
import Vuex from 'vuex'

import login from "./modules/login";
import play from "./modules/play";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    login, play
  }
})
