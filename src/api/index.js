import axios from "axios";

const myaxios = axios.create({
    baseURL:"http://localhost:3000"
})

export function getRecommend(){
    return myaxios({
        url:"/personalized",
        params:{
            limit: 6
        }
    })
}

export function getNewSong(){
    return myaxios({
        url:"/personalized/newsong"
    })
}
export function getBanner(){
    return myaxios({
        url:"/banner",
        params:{
            type:2
        }
    })
}

export function getTopList(){
    return myaxios({
        url:"/toplist"
    })
}

export function getHotSong(id){
    return myaxios({
        url:"/playlist/detail",
        params:{
            id
        }
    })
}

export function getSearchHotKey(){
    return myaxios({
        url:"/search/hot"
    })
}

export function getSearch(ops){
    return myaxios({
        url:"/search",
        params:ops
    })
}

export function getSearchSlideDown(kw){
    return myaxios({
        url:"/search/suggest",
        params:{
            keywords:kw,
            type:"mobile"
        }
    })
}

export function getNickNameRepeat(nn){
    return myaxios({
        url:"/nickname/check",
        params:{
            nickname: nn
        }
    })
}

export function getTelRepeat(tel){
    return myaxios({
        url:"/cellphone/existence/check",
        params:{
            phone: tel
        }
    })
}

export function getSMS(tel){
    return myaxios({
        url:"/captcha/sent",
        params:{
            phone: tel
        }
    })
}

export function getSMSisRight(tel, sms){
    return myaxios({
        url:"/captcha/verify",
        params:{
            phone: tel,
            captcha: sms
        }
    })
}

export function telRegister(ops){
    return myaxios({
        url:"/register/cellphone",
        params:ops
    })
}

export function telLogin(ops){
    return myaxios({
        url:"/login/cellphone",
        params:ops
    })
}

export function loginStatus(){
    return myaxios({
        url:"/login/status"
    })
}

export function getUserDetail(id){
    return myaxios({
        url:"/user/detail",
        params:{
            uid: id
        }
    })
}

export function getAccount(){
    return myaxios({
        url:"/user/account"
    })
}

export function getSub(){
    return myaxios({
        url:"/user/subcount"
    })
}

export function getLv(){
    return myaxios({
        url:"/user/level"
    })
}

export function getPlayList(id){
    return myaxios({
        url:"/user/playlist",
        params:{
            uid: id
        }
    })
}

export function getPlayHistory(id){
    return myaxios({
        url:"/user/record",
        params:{
            uid: id
        }
    })
}

export function getSongList(id){
    return myaxios({
        url:"/playlist/track/all",
        params:{
            id: id
        }
    })
}

export function getSong(id){
    return myaxios({
        url:"/song/url",
        params:{
            id: id
        }
    })
}

export function getSongDetail(id){
    return myaxios({
        url:"/song/detail",
        params:{
            ids: id
        }
    })
}

export function getLyric(id){
    return myaxios({
        url:"/lyric",
        params:{
            id: id
        }
    })
}