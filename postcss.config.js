module.exports = {
  plugins: {
    // rem
    "postcss-pxtorem": {
      rootValue: 37.5, //设计稿的尺寸375
      // rootValue: 75, //设计稿的尺寸750
      propList: ["*"], // 需要转换的属性，这里选择全部都进行转换
    }

    // vp
    //   'postcss-px-to-viewport': {
    //     viewportWidth: 375,
  }
}